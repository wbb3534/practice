package com.example.practice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RankItem {
    private String rank;
    private String movieThumbUrl;
    private String movieName;
    private String grade;
}
