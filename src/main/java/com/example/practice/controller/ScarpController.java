package com.example.practice.controller;

import com.example.practice.model.RankItem;
import com.example.practice.service.ScrapService;
import lombok.RequiredArgsConstructor;
import org.jsoup.nodes.Element;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/movie")
@RequiredArgsConstructor
public class ScarpController {
    private final ScrapService scrapService;

    @GetMapping("/rank")
    public List<RankItem> getHtml() throws IOException {
        List<RankItem> result = scrapService.run();
        return result;
    }
}
