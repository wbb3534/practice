package com.example.practice.service;


import com.example.practice.model.RankItem;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
public class ScrapService {
    private Document getFullHtml() throws IOException {
        String url = "https://search.naver.com/search.naver?where=nexearch&query=%eb%b0%95%ec%8a%a4%ec%98%a4%ed%94%bc%ec%8a%a4%ec%88%9c%ec%9c%84";
        Connection connection = Jsoup.connect(url);

        Document document = connection.get();
        return  document;
    }

    private List<Element> parseHtml(Document document) {
        Elements elements = document.getElementsByClass("_panel_rating _tab_content");
        List<Element> result = new LinkedList<>();
        for (Element item : elements) {
            Elements lis = item.getElementsByTag("li");
            for (Element item2 : lis) {
                result.add(item2);
            }
        }
        return result;
    }

    private List<RankItem> makeHtml(List<Element> list) {
        List<RankItem> result = new LinkedList<>();
        for (Element item : list) {
            String movieThumbUrl = item.getElementsByClass("thumb").get(0).getElementsByTag("img").get(0).attr("src");
            String movieName = item.getElementsByClass("name").get(0).text();
            String grade = item.getElementsByClass("sub_text").get(0).text();
            String rank = item.getElementsByClass("this_text").get(0).text();

            RankItem addItem = new RankItem();

            addItem.setMovieThumbUrl(movieThumbUrl);
            addItem.setMovieName(movieName);
            addItem.setRank(rank);
            addItem.setGrade(grade);


            result.add(addItem);
        }
        return result;
    }

    public List<RankItem> run() throws IOException {
        Document document = getFullHtml();
        List<Element> elements = parseHtml(document);
        List<RankItem> result = makeHtml(elements);
        return result;
    }
}
